============
What Is toe?
============

``toe`` is a script to install a basic Arch Linux system. This is a **very
destructive** script, so be sure to have a backup of anything you're interested
in keeping around. **The script will wipe the partition table of your /dev/sda
device without asking.**

Things are currently designed for brand new systems and VMs.

Simply run the following command after booting up the Arch Linux install
environment::

    wget https://bitbucket.org/instarch/toe/raw/master/toe.sh && bash toe.sh

It will download and run everything it needs to **wipe**, partition, format,
install, and configure your new Arch system.

Why "toe"?
==========

A lot of people ask me why I called this project ``toe``. It's a good question,
for sure. My 10-second thought process when coming up with the name went
something like this:

* bootstrap? nah--too cliche
* shoelace? meh
* flip flop? nah--too unrelated
* toe thong? nah--too long
* toe! not cliche, only a little meh, completely unrelated, but perfectly lazy.

And so it was/is/will be.

Options
=======

This script has a few options that you may use to change its behavior:

* You may specify a root password by including a ``PASSWORD`` environment
  variable. For example::

    wget https://bitbucket.org/instarch/toe/raw/master/toe.sh && PASSWORD=foo bash toe.sh

* You may instruct the script to automatically reboot the system when
  installation finishes with a ``REBOOT`` environment variable::

    wget https://bitbucket.org/instarch/toe/raw/master/toe.sh && REBOOT=y bash toe.sh

* You may select a specific mirror using the ``MIRROR`` environment variable::

    wget https://bitbucket.org/instarch/toe/raw/master/toe.sh && MIRROR=K bash toe.sh

  Pre-configured mirror keys are:

  * ``A``: http://mirrors.acm.wpi.edu/archlinux/$repo/os/$arch
  * ``K``: http://mirrors.kernel.org/archlinux/$repo/os/$arch
  * ``LQ``: http://mirrors.liquidweb.com/archlinux/$repo/os/$arch
  * ``LW``: http://mirror.us.leaseweb.net/archlinux/$repo/os/$arch

  If this environment variable is not specified (or has an invalid key),
  reflector_ will be installed to automatically determine which mirrors appear
  to be good, fast choices.

Credits
=======

While I did write my own set of scripts to install Arch Linux back in 2012, the
current iteration is greatly inspired by an install script that I found for
installing Arch using Packer_. I don't recall exactly which script inspired me,
but `elasticdog's`_ version seems like it might well be the one.

.. _Packer: http://www.packer.io/
.. _elasticdog's: https://github.com/elasticdog/packer-arch
.. _reflector: https://wiki.archlinux.org/index.php/Reflector
