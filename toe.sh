#!/usr/bin/env bash

DISK='/dev/sda'
FQDN='archvm'
KEYMAP='us'
LANGUAGE='en_US.UTF-8'
[[ "${PASSWORD}" != "" ]] && PASSWORD=$(openssl passwd -crypt "${PASSWORD}")
TIMEZONE='UTC'

MIRRORLIST=/etc/pacman.d/mirrorlist
CONFIG_SCRIPT='/usr/local/bin/arch-config.sh'
BOOT_PARTITION="${DISK}1"
ROOT_PARTITION="${DISK}2"
SWAP_PARTITION="${DISK}3"
TARGET_DIR='/mnt'
BOOT_DIR=${TARGET_DIR}/boot

# some commonly-used, fast mirrors
declare -A MIRRORS
MIRRORS[A]='http://mirrors.acm.wpi.edu/archlinux/$repo/os/$arch'
MIRRORS[K]='http://mirrors.kernel.org/archlinux/$repo/os/$arch'
MIRRORS[LQ]='http://mirrors.liquidweb.com/archlinux/$repo/os/$arch'
MIRRORS[LW]='http://mirror.us.leaseweb.net/archlinux/$repo/os/$arch'
MIRRORS[H]='http://mirror.vanzclan.net/archlinux/$repo/os/$arch'
MIRROR=${MIRRORS[${MIRROR}]:-${MIRROR}}

set -e

if $(hash sgdisk) && $(hash pacman) && $(hash pacstrap) && $(hash genfstab) && $(hash arch-chroot); then
    echo "...all commands present and accounted for..."
else
    cat <<-EOF
This script is intended to be used from an Arch Linux system with the
arch-install-scripts package installed. I did not detect the utilities I
require, so I'm going to assume you've been conned into running this script
without checking what it's supposed to do.

Good day!
EOF
    exit 1
fi

clear
cat <<-EOF
################################################################################
 WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING
################################################################################

                        This script is designed to

  ██████╗ ██████╗ ██╗     ██╗████████╗███████╗██████╗  █████╗ ████████╗███████╗
 ██╔═══██╗██╔══██╗██║     ██║╚══██╔══╝██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
 ██║   ██║██████╔╝██║     ██║   ██║   █████╗  ██████╔╝███████║   ██║   █████╗
 ██║   ██║██╔══██╗██║     ██║   ██║   ██╔══╝  ██╔══██╗██╔══██║   ██║   ██╔══╝
 ╚██████╔╝██████╔╝███████╗██║   ██║   ███████╗██║  ██║██║  ██║   ██║   ███████╗
  ╚═════╝ ╚═════╝ ╚══════╝╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚══════╝

                     absolutely EVERYTHING on ${DISK}!!

    If this is not what you desire, hit Control-C RIGHT NOW. Bad things will
                    happen if you proceed! BAD THINGS!!

################################################################################
 WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING ! WARNING
################################################################################


EOF

read -p "Press [Enter] to proceed with obliteration!"

echo "==> clearing partition table on ${DISK}"
sgdisk --zap ${DISK}

echo "==> destroying magic strings and signatures on ${DISK}"
dd if=/dev/zero of=${DISK} bs=512 count=2048
wipefs --all ${DISK}

echo "==> creating partitions on ${DISK}"
sgdisk --new=1:0:+100M \
       --new=2:0:-1G \
       --new=3:0:0 \
       --typecode=3:8200 \
       --attributes=1:set:2 ${DISK}

echo '==> creating filesystems (ext4)'
mkfs.ext4 -F -m 0 -q -L boot ${BOOT_PARTITION}
mkfs.ext4 -F -m 0 -q -L root ${ROOT_PARTITION}

echo '==> creating swap filesystem'
mkswap ${SWAP_PARTITION}

echo "==> mounting ${ROOT_PARTITION} to ${TARGET_DIR}"
mount -o noatime,errors=remount-ro ${ROOT_PARTITION} ${TARGET_DIR}

echo "==> mounting ${BOOT_PARTITION} to ${BOOT_DIR}"
mkdir -p ${BOOT_DIR}
mount -o noatime,errors=remount-ro ${BOOT_PARTITION} ${BOOT_DIR}

echo "==> turning on swap"
swapon ${SWAP_PARTITION}

if [[ "${MIRROR}" != "" ]]; then
    echo "==> Using mirror: ${MIRROR}"
    echo "Server = ${MIRROR}" > ${MIRRORLIST}
fi

echo '==> updating pacman...'
pacman -Sy --needed --noconfirm pacman

if [[ "${MIRROR}" == "" ]]; then
    echo '==> finding fastest mirrors...'
    pacman -Sy --needed --noconfirm reflector
    reflector -l 10 -f 10 -p http -n 5 --sort rate --save ${MIRRORLIST}
fi

echo '==> bootstrapping the base installation'
pacstrap ${TARGET_DIR} base base-devel openssh syslinux tmux vim zsh gptfdisk
arch-chroot ${TARGET_DIR} syslinux-install_update -iam
sed -i 's/sda3/sda2/' "${TARGET_DIR}/boot/syslinux/syslinux.cfg"
sed -i 's/TIMEOUT 50/TIMEOUT 10/' "${TARGET_DIR}/boot/syslinux/syslinux.cfg"

echo '==> generating the filesystem table'
genfstab -p ${TARGET_DIR} >> "${TARGET_DIR}/etc/fstab"

echo '==> generating the system configuration script'
install --mode=0755 /dev/null "${TARGET_DIR}${CONFIG_SCRIPT}"

cat <<-EOF > "${TARGET_DIR}${CONFIG_SCRIPT}"
    echo '${FQDN}' > /etc/hostname
    ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
    echo 'KEYMAP=${KEYMAP}' > /etc/vconsole.conf
    sed -i 's/#${LANGUAGE}/${LANGUAGE}/' /etc/locale.gen
    locale-gen
    mkinitcpio -p linux

    [[ "${PASSWORD}" != "" ]] && usermod --password ${PASSWORD} root

    # https://wiki.archlinux.org/index.php/Network_Configuration#Device_names
    ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules

    sed -i 's/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config
    systemctl enable sshd.service dhcpcd@eth0.service
    pacman -Scc --noconfirm
EOF

echo '==> entering chroot and configuring system'
arch-chroot ${TARGET_DIR} ${CONFIG_SCRIPT}
rm "${TARGET_DIR}${CONFIG_SCRIPT}"

echo '==> installation complete!'
umount -R ${TARGET_DIR}
swapoff ${SWAP_PARTITION}

[[ "${REBOOT}" != "" ]] && systemctl reboot || exit 0
